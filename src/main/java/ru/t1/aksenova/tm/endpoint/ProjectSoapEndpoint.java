package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.dto.soap.*;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;

@Endpoint
public class ProjectSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://aksenova.t1.ru/tm/dto/soap";

    @Autowired
    private IProjectDTOService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = NAMESPACE)
    public ProjectAddResponse add(@RequestPayload final ProjectAddRequest request) {
        @NotNull final ProjectAddResponse response = new ProjectAddResponse();
        @Nullable final ProjectDTO project = new ProjectDTO("New Project " + System.currentTimeMillis(), Status.NOT_STARTED);
        projectService.add(project);
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        @NotNull final ProjectClearResponse response = new ProjectClearResponse();
        projectService.clear();
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        @NotNull final ProjectCountResponse response = new ProjectCountResponse();
        response.setCount(projectService.getSize());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        @NotNull final ProjectDeleteResponse response = new ProjectDeleteResponse();
        @Nullable final ProjectDTO project = request.getProject();
        projectService.remove(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        @NotNull final ProjectDeleteByIdResponse response = new ProjectDeleteByIdResponse();
        @Nullable final String id = request.getId();
        projectService.removeOneById(id);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@RequestPayload final ProjectExistsByIdRequest request) {
        @NotNull final ProjectExistsByIdResponse response = new ProjectExistsByIdResponse();
        @Nullable final String id = request.getId();
        response.setExists(projectService.existsById(id));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjects(projectService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = projectService.findOneById(id);
        if (project != null)
            response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        @NotNull final ProjectSaveResponse response = new ProjectSaveResponse();
        @Nullable final ProjectDTO project = request.getProject();
        projectService.update(project);
        response.setProject(project);
        return response;
    }

}
