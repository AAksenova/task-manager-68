package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Override
    @PostMapping("/add")
    public TaskDTO add(@RequestBody final @NotNull TaskDTO task) {
        return taskService.add(task);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @Override
    @PostMapping("/save")
    public void save(@RequestBody final @NotNull TaskDTO task) {
        taskService.update(task);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") final @NotNull String id) {
        return taskService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") final @NotNull String id) {
        return (taskService.findOneById(id) != null);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskService.getSize();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") final @NotNull String id) {
        taskService.removeOneById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final @NotNull TaskDTO task) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear() {
        taskService.clear();
    }

}