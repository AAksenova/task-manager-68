package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Override
    @PostMapping("/add")
    public ProjectDTO add(@RequestBody final @NotNull ProjectDTO project) {
        return projectService.add(project);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @Override
    @PostMapping("/save")
    public void save(@RequestBody final @NotNull ProjectDTO project) {
        projectService.update(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") final @NotNull String id) {
        return projectService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") final @NotNull String id) {
        ProjectDTO p = projectService.findOneById(id);
        return (projectService.findOneById(id) != null);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.getSize();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") final @NotNull String id) {
        projectService.removeOneById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final @NotNull ProjectDTO project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear() {
        projectService.clear();
    }

}
