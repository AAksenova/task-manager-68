package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.dto.soap.*;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;

@Endpoint
public class TaskSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://aksenova.t1.ru/tm/dto/soap";

    @Autowired
    private ITaskDTOService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskAddRequest", namespace = NAMESPACE)
    public TaskAddResponse add(@RequestPayload final TaskAddRequest request) {
        @NotNull final TaskAddResponse response = new TaskAddResponse();
        @Nullable final TaskDTO task = new TaskDTO("New Task " + System.currentTimeMillis(), Status.NOT_STARTED);
        taskService.add(task);
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        @NotNull final TaskClearResponse response = new TaskClearResponse();
        taskService.clear();
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) {
        @NotNull final TaskCountResponse response = new TaskCountResponse();
        response.setCount(taskService.getSize());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        @NotNull final TaskDeleteResponse response = new TaskDeleteResponse();
        @Nullable final TaskDTO task = request.getTask();
        taskService.remove(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        @NotNull final TaskDeleteByIdResponse response = new TaskDeleteByIdResponse();
        @Nullable final String id = request.getId();
        taskService.removeOneById(id);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(@RequestPayload final TaskExistsByIdRequest request) {
        @NotNull final TaskExistsByIdResponse response = new TaskExistsByIdResponse();
        @Nullable final String id = request.getId();
        response.setExists(taskService.existsById(id));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTasks(taskService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.findOneById(id);
        if (task != null)
            response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        @NotNull final TaskSaveResponse response = new TaskSaveResponse();
        @Nullable final TaskDTO task = request.getTask();
        taskService.update(task);
        response.setTask(task);
        return response;
    }

}
