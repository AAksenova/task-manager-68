package ru.t1.aksenova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.aksenova.tm.config.ApplicationConfiguration;
import ru.t1.aksenova.tm.config.WebApplicationConfiguration;
import ru.t1.aksenova.tm.config.WebConfig;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class, WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(@NotNull final ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }


}
