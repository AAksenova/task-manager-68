package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;

import javax.transaction.Transactional;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)

public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @Transactional
    void deleteByProjectId(@NotNull String projectId);

}
