package ru.t1.aksenova.tm.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@JsonIgnoreProperties(ignoreUnknown = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDTO", propOrder = {
        "id",
        "created",
        "name",
        "description",
        "status"
})
public final class ProjectDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @XmlElement(required = true)
    @Column(length = 100, nullable = false)
    private String name = "";

    @NotNull
    @Column(length = 255)
    private String description = "";

    @NotNull
    @Column(length = 30)
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;


    @NotNull
    @Column(updatable = false, nullable = false)
    @XmlSchemaType(name = "dateTime")
    private Date created = new Date();

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

}