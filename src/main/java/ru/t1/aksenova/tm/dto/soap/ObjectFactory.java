package ru.t1.aksenova.tm.dto.soap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
@NoArgsConstructor
public class ObjectFactory {

    @NotNull
    public ProjectAddRequest createProjectAddRequest() {
        return new ProjectAddRequest();
    }

    @NotNull
    public ProjectAddResponse createProjectAddResponse() {
        return new ProjectAddResponse();
    }

    @NotNull
    public ProjectClearRequest createProjectClearRequest() {
        return new ProjectClearRequest();
    }

    @NotNull
    public ProjectClearResponse createProjectClearResponse() {
        return new ProjectClearResponse();
    }

    @NotNull
    public ProjectCountRequest createProjectCountRequest() {
        return new ProjectCountRequest();
    }

    @NotNull
    public ProjectCountResponse createProjectCountResponse() {
        return new ProjectCountResponse();
    }

    @NotNull
    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    @NotNull
    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    @NotNull
    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    @NotNull
    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    @NotNull
    public ProjectExistsByIdRequest createProjectExistsByIdRequest() {
        return new ProjectExistsByIdRequest();
    }

    @NotNull
    public ProjectExistsByIdResponse createProjectExistsByIdResponse() {
        return new ProjectExistsByIdResponse();
    }

    @NotNull
    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    @NotNull
    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    @NotNull
    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    @NotNull
    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    @NotNull
    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    @NotNull
    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    @NotNull
    public TaskAddRequest createTaskAddRequest() {
        return new TaskAddRequest();
    }

    @NotNull
    public TaskAddResponse createTaskAddResponse() {
        return new TaskAddResponse();
    }

    @NotNull
    public TaskClearRequest createTaskClearRequest() {
        return new TaskClearRequest();
    }

    @NotNull
    public TaskClearResponse createTaskClearResponse() {
        return new TaskClearResponse();
    }

    @NotNull
    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    @NotNull
    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    @NotNull
    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    @NotNull
    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    @NotNull
    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    @NotNull
    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    @NotNull
    public TaskExistsByIdRequest createTaskExistsByIdRequest() {
        return new TaskExistsByIdRequest();
    }

    @NotNull
    public TaskExistsByIdResponse createTaskExistsByIdResponse() {
        return new TaskExistsByIdResponse();
    }

    @NotNull
    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    @NotNull
    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    @NotNull
    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    @NotNull
    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    @NotNull
    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    @NotNull
    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

}
