package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.repository.dto.ProjectDTORepository;

import java.util.Collection;
import java.util.List;

public interface IProjectDTOService {

    @NotNull
    ProjectDTORepository getRepository();

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model);

    void update(@NotNull ProjectDTO model);

    void remove(@NotNull ProjectDTO model);

    @NotNull
    ProjectDTO create(@Nullable ProjectDTO project);

    @NotNull
    ProjectDTO create(
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    List<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findOneById(
            @Nullable String id
    );

    @Transactional
    void clear();

    @Transactional
    void removeAll();

    @Nullable
    ProjectDTO removeOne(
            @Nullable ProjectDTO project
    );

    @Nullable
    ProjectDTO removeOneById(
            @Nullable String id
    );

    @NotNull ProjectDTO updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull ProjectDTO changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String id);

    long getSize();
}
