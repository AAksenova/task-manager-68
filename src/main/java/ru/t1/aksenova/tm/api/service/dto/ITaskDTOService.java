package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.repository.dto.TaskDTORepository;

import java.util.Collection;
import java.util.List;

public interface ITaskDTOService {
    @NotNull TaskDTORepository getRepository();

    @NotNull
    @Transactional
    TaskDTO add(@NotNull TaskDTO task);

    @Transactional
    void update(@NotNull TaskDTO task);

    @Transactional
    void remove(@NotNull TaskDTO task);

    @NotNull TaskDTO create(@Nullable TaskDTO task);

    @NotNull TaskDTO create(
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks);

    @NotNull List<TaskDTO> findAll();

    @Nullable TaskDTO findOneById(
            @Nullable String id
    );

    @NotNull void removeByProjectId(
            @Nullable String projectId
    );

    @Transactional
    void clear();

    @Transactional
    void removeAll();

    @Nullable TaskDTO removeOne(
            @Nullable TaskDTO task
    );

    @Nullable TaskDTO removeOneById(
            @Nullable String id
    );

    @NotNull TaskDTO updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull TaskDTO changeTaskStatusById(
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String id);

    long getSize();
}
