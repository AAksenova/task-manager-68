package ru.t1.aksenova.tm.exception.field;

public final class DateEmptyException extends AbstractFieldException {

    public DateEmptyException() {
        super("Error! Date is empty..");
    }

}
