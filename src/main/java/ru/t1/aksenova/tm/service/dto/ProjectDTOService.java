package ru.t1.aksenova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.repository.dto.ProjectDTORepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService implements IProjectDTOService {

    @Nullable
    @Autowired
    private ProjectDTORepository repository;

    @NotNull
    @Override
    public ProjectDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@NotNull final ProjectDTO project) {
        getRepository().save(project);
        return project;
    }

    @Override
    @Transactional
    public void update(@NotNull ProjectDTO project) {
        getRepository().save(project);
    }

    @Override
    @Transactional
    public void remove(@NotNull ProjectDTO project) {
        getRepository().delete(project);
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        add(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects) {
        if (projects.isEmpty()) return Collections.emptyList();
        projects.forEach(getRepository()::save);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<ProjectDTO> project = getRepository().findById(id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return project.orElse(null);
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void removeAll() {
        getRepository().deleteAll();
    }

    @Nullable
    @Override
    public ProjectDTO removeOne(
            @Nullable final ProjectDTO project
    ) {
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO removeOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        remove(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findById(id).orElse(null) != null;
    }

    @Override
    public long getSize() {
        return getRepository().count();
    }

}
