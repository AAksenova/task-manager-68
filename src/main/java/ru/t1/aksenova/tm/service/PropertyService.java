package ru.t1.aksenova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    public static final String GIT_COMMITTER_NAME1 = "gitCommitterName";

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.url']}")
    private String databaseConnection;

    @Value("#{environment['database.dialect']}")
    private String databaseHibernateDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHibernateHbm2ddl;

    @Value("#{environment['database.show_sql']}")
    private String databaseHibernateShowSql;

    @Value("#{environment['database.format_sql']}")
    private String databaseHibernateFormatSql;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseCacheUseSecondLevelCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseCacheUseMinimalPuts;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseCacheUseQueryCache;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheProviderConfigFileResourcePath;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactoryClass;

    @Value("#{environment['database.init.token']}")
    private String databaseInitToken;

    @Value("#{environment['liquibase.config.filename']}")
    private String liquibasePropertiesFilename;

}