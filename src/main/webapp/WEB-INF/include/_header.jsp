<html>
    <head>
        <title>TASK MANAGER</title>
    </head>
    <style>
        h1 {
            font-size: 1.6em;
        }
        a {
            color: darkblue;
        }
        select {
            width: 200px;
        }
        input[type="text"] {
            width: 200px;
        }
        input[type="date"] {
            width: 200px;
        }
    </style>
    <body>
        <table width="100%" height="100%" border="1" style="border-collapse: collapse">
            <tr>
                <td height="35" width="200" nowrap="nowrap" align="center">
                    <b>TASK MANAGER</b>
                </td>
                <td width="100%" align="right">
                    <a href="/projects">PROJECTS</a>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="/tasks">TASKS</a>
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" height="100%" valign="top" style="padding: 10px">
